#!/usr/bin/env python
from __future__ import print_function
import sys
import re
from random import choice, randint
from string import ascii_lowercase as ascii

DICTIONARY='/usr/share/dict/words'

class WordDecider(object):
    def __init__(self):
        self.load_words()

    def load_words(self):
        with open(DICTIONARY, 'r') as f:
            self.wordlist = [re.sub(r'[^a-zA-Z]', '', w).lower() for w in f]
            self.words = set(self.wordlist)

    def decide(self, candidate):
        return candidate in self.words

    def genpos(self):
        return self.gen_positive()

    def gen_positive(self):
        return ''.join(choice(self.wordlist) for _ in xrange(20))

    def genneg(self):
        return self.gen_negative()

    def gen_negative(self):
        return ''.join(choice("0123456789") for _ in xrange(randint(100,200)))

     
def main():
    wd = WordDecider()

    for _ in xrange(10):
        pos = wd.genpos()
        print(pos)
    for _ in xrange(10):
        neg = wd.genneg()
        print(neg)

if __name__ == '__main__':
    main()
